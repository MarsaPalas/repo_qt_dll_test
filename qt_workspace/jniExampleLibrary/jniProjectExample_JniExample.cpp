#include "jniProjectExample_JniExample.h"
//#include <iostream>
//#include <cstring>

JNIEXPORT void JNICALL Java_jniProjectExample_JniExample_callJavaMethod (JNIEnv * env, jobject obj) {
    jclass jniExampleCls = env->GetObjectClass(obj);

    jmethodID mid = env->GetMethodID(jniExampleCls, "printSomething", "()V");

    env->CallVoidMethod(obj, mid);
}

JNIEXPORT jobject JNICALL Java_jniProjectExample_JniExample_createAndReturnBean  (JNIEnv *env, jobject obj) {
    jclass beanClass = env->FindClass("jniProjectExample/Bean");
    jmethodID constructorMethodId = env->GetMethodID(beanClass, "<init>", "()V");
    jobject bean = env->NewObject(beanClass, constructorMethodId);

    jstring newString = env->NewStringUTF("this bean has been created in Java_jniProjectExample_JniExample_createAndReturnBean");

    jmethodID setStringMid = env->GetMethodID(beanClass, "setDataString", "(Ljava/lang/String;)V");

    env->CallVoidMethod(bean, setStringMid, newString );

    return bean;
}

JNIEXPORT void JNICALL Java_jniProjectExample_JniExample_modifyBean  (JNIEnv *env, jobject obj, jobject bean) {
    jclass beanCls = env->GetObjectClass(bean);

    jmethodID setStringMid = env->GetMethodID(beanCls, "setDataString", "(Ljava/lang/String;)V");

    // set the bean member "dataString"
    jstring newString = env->NewStringUTF("world");
    env->CallVoidMethod(bean, setStringMid, newString );

    // set the bean member "dataByteArray"
    jbyteArray newByteArray = env->NewByteArray(5);
    jbyte buffer[5] = {5,4,3,2,1};
    env->SetByteArrayRegion( newByteArray, 0, 5, buffer);

    jmethodID setByteArrayMid = env->GetMethodID(beanCls, "setDataByteArray", "([B)V");

    env->CallVoidMethod(bean, setByteArrayMid, newByteArray );
/*
    char bla[] = "etf";
    if(strcmp(bla, "label") == 0){
        std::cout<<"strcmp 1";
    }
    else {
        std::cout<<"strcmp 2";
    } */
}


JNIEXPORT void JNICALL Java_jniProjectExample_JniExample_crashTheJvm (JNIEnv *env, jobject obj) {
    //stack overflow - http://en.wikipedia.org/wiki/Stack_overflow
    double x[1000000];
}
