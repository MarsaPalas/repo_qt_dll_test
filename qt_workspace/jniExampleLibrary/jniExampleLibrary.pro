#-------------------------------------------------
#
# Project created by QtCreator 2019-03-23T09:43:24
#
#-------------------------------------------------

QT       -= core gui

TARGET = jniExampleLibrary
TEMPLATE = lib

DEFINES += JNIEXAMPLELIBRARY_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


QMAKE_CFLAGS += -std=c++11

macx:INCLUDEPATH += /Library/Java/JavaVirtualMachines/jdk1.8.0_60.jdk/Contents/Home/include \
    /Library/Java/JavaVirtualMachines/jdk1.8.0_60.jdk/Contents/Home/include/darwin

win32:INCLUDEPATH +=    "C:\Program Files\Java\jdk1.8.0_181\include" \
                        "C:\Program Files\Java\jdk1.8.0_181\include\win32"

SOURCES += \
    jniProjectExample_JniExample.cpp

HEADERS += \
    jni.h \
    jni_md.h \
    jniProjectExample_JniExample.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
