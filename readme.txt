This is an example of using Java Native Interface (JNI) to use C++ code from Java.

Project qt_workspace/jniExampleLibrary is a QT project written in C++ for building jniExampleLibrary.dll.
Project eclipse_workspace/jniProjectExample is an Eclipse project written in Java which uses previously generated dll.

In order for this to work, in qt_workspace/jniExampleLibrary.pro set correct paths to jni.h and jni_mdt.h files
and in eclipse project set java.library.path for the generated dll.
